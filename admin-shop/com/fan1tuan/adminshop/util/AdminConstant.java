package com.fan1tuan.adminshop.util;

public class AdminConstant {
	
	//店铺列表-分页大小
	public static final int SHOPLIST_PAGESIZE = 30;
	//菜品列表-分页大小
	public static final int DISHLIST_PAGESIZE = 30;
	//订单列表-分页大小
	public static final int ORDERLIST_PAGESIZE = 50;
	
	//店铺头像上传路径
	public static final String UPLOAD_SHOP_PATH = "/uploads/shop_img";
	//菜品头像上传路径
	public static final String  UPLOAD_DISH_PATH = "/uploads/dish_img";
	//图片保存的前缀，方便页面取出来
	public static final String IMG_SAVE_PREFFIX = "http://localhost:8080";

}
